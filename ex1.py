import search
import random
import math
import json


ids = ["318728490", "205451123"]


# TODO list:
# TODO implement actions()
# TODO implement h2
# TODO create admissible h


def to_dict(state):
    state = json.loads(state)
    return state


def to_str(state):
    state = json.dumps(state)
    return state


def generate_state(initial: dict):
    # Add picked_up attribute to each taxi
    for taxi in initial["taxis"].keys():
        initial["taxis"][taxi]["picked_up"] = []
        initial["taxis"][taxi]["max_fuel"] = initial["taxis"][taxi]["fuel"]  # could be used in h

    # Add in_taxi attribute to aid in coding later
    for passenger in initial["passengers"].keys():
        initial["passengers"]["passenger"]["in_taxi"] = False

    initial["num_delivered"] = 0
    return to_str(initial)


def validate_action(action):
    pick_up_package_loc = [tup[2] for tup in action if tup[0] == "pick up"]
    if len(set(pick_up_package_loc)) != len(pick_up_package_loc):
        return False
    return True


class TaxiProblem(search.Problem):
    """This class implements a medical problem according to problem description file"""

    def __init__(self, initial):
        """Don't forget to implement the goal test
        You should change the initial to your own representation.
        search.Problem.__init__(self, initial) creates the root node"""
        self.map = initial.pop("map")  # remove map since it is unnecessary to track, it is static
        self.num_passengers = len(initial["passengers"].keys())
        initial_state = generate_state(initial)
        search.Problem.__init__(self, initial_state)
        print(initial)
        # self.taxis = initial["taxis"]


    """Returns all the actions that can be executed in the given
    state. The result should be a tuple (or other iterable) of actions
    as defined in the problem description file"""
    def actions(self, state):
        """
        Look at current state, get all possible actions from it
        Then remove all illegal actions (we're left with only verified actions)
        And prepare actions for return
        @param state: json str of current state (dict)
        return: TODO understand how to return the actions
        TODO make sure only one taxi is on one tile
        """
        # final return value should be all validate combinations of action from
        # every drone: ( ((act1_d1),(act1,d2)...(act1_dn)), ((act2_d1), (act1_d2), ...), ...  )
        print(state)
        # Addition

    """Return the state that results from executing the given
            action in the given state. The action must be one of
            self.actions(state)."""
    def result(self, state, multi_action):
        """
        Consider current state and action, in order to perform it
        Assuming it is already legal since we checked during creation
        @param state: json str of current state (dict)
        @param multi_action: Assume we get some iterable of actions
            which are of the form (command, taxi, ?optional)
        return:
        """
        state = to_dict(state)
        for action in multi_action:
            command = action[0]
            taxi = action[1]
            if command == "move":
                location = action[2]
                state["taxis"][taxi]["location"] = location  # TODO verify if list or tuple
            elif command == "pick up":
                passenger = action[2]  # passenger name
                state["taxis"][taxi]["picked_up"].append(passenger)  # add passenger to taxi
                state["passengers"][passenger]["in_taxi"] = True
            elif command == "drop off":  # can only be dropped off at destination, no need to verify
                passenger = action[2]
                state["taxis"][taxi]["picked_up"].remove(passenger)  # remove passenger from taxi
                state["passengers"][passenger]["in_taxi"] = False  # update passenger ride status
                passenger_destination = state["passengers"][passenger]["destionation"]
                state["passengers"][passenger]["location"] = passenger_destination  # update passenger location
                state["num_delivered"] += 1
            elif command == "refuel":
                max_capacity = state["taxis"][taxi]["max_fuel"]
                state["taxis"][taxi]["fuel"] = max_capacity
            elif command == "wait":
                pass
        return to_dict(state)

    def goal_test(self, state):
        """ Given a state, checks if this is the goal state.
         Returns True if it is, False otherwise."""
        state = to_dict(state)
        return state["num_delivered"] == self.num_passengers

    def h(self, node):
        """ This is the heuristic. It gets a node (not a state,
        state can be accessed via node.state)
        and returns a goal distance estimate"""
        return self.h_1(node)

    def h_1(self, node):
        """
        This is a simple heuristic
        """
        state = to_dict(node.state)
        num_unpicked_passengers = sum([1 for passenger in state["passengers"].keys()
                                       if state["passengers"][passenger]["in_taxi"] == False])
        num_picked_undelivered = 0
        for passenger in state["passengers"].keys():
            if state["passengers"][passenger]["in_taxi"]:
                if state["passengers"][passenger]["location"] != state["passengers"][passenger]["destination"]:
                    num_picked_undelivered += 1

        num_taxis = len(state["taxis"].keys())

        return (num_unpicked_passengers * 2 + num_picked_undelivered) / num_taxis

    def h_2(self, node):
        """
        This is a slightly more sophisticated Manhattan heuristic
        """

    """Feel free to add your own functions
    (-2, -2, None) means there was a timeout"""


def create_taxi_problem(game):
    return TaxiProblem(game)

